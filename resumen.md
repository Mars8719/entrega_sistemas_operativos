# Resumen del libro (Sistemas operativos modernos).
## Autor: Andrew S. Tanaenbaum
## Historia de los sistemas operativos
- Los sistemas operativos han ido evolucionando a través de los años. En las siguientes secciones se analizara brevemente algunos de los hitos mas 
importantes. Como los sistemas operativos han estado estrechamente relacionados a través de la historia con la arquitectura de las computadoras en 
las que se ejecutan donde se analizran generaciones sucesivas de computadoras para ejemplificar como eran sus sistemas operativos. Esto se hace con 
el proposito de proporcionar cierta estructura donde de otra manera no habría.

**1. La primera generación (1945 a 1955): tubos al vacio**
- Durante la Segunda Guerra Mundial, se impulsó la creación de computadoras digitales. El profesor John Atanasoff y Clifford Berry construyeron la 
primera computadora digital funcional en Iowa State University, utilizando 300 tubos de vacío. En todo el mundo, se desarrollaron otras 
computadoras, aunque todas eran primitivas y lentas en sus cálculos. La programación se realizaba en lenguaje máquina o mediante la conexión manual 
de cables. La introducción de tarjetas perforadas a principios de la década de 1950 mejoró la rutina al permitir la escritura y lectura de programas 
de manera más eficiente.

**2. La segundo generación (1955 a 1965): transistores y sistemas de procesamiento por lotes**
- La introducción del transistor en la década de 1950 marcó un cambio significativo en la computación. Las computadoras se volvieron más confiables, 
lo que permitió su fabricación y venta a clientes dispuestos a invertir en ellas. Surgió una distinción entre diseñadores, constructores, 
operadores, programadores y personal de mantenimiento. Estas computadoras, conocidas como mainframes, se alojaban en salas especiales con aire 
acondicionado y operadores profesionales. Los programadores escribían programas en papel y luego los convertían en tarjetas perforadas. Se 
implementó el sistema de procesamiento por lotes para mejorar la eficiencia. La IBM 1401, aunque no era ideal para cálculos numéricos, desempeñó un 
papel clave en esta evolución.

**3. La tercera generacion (1965 a 1980): circuitos integrados y multiprogramación**
- En la década de 1960, IBM revolucionó la computación con la línea System/360, que ofrecía una amplia variedad de modelos compatibles en software, 
aunque esto generó un sistema operativo complejo llamado OS/360. La tercera generación de sistemas operativos introdujo la multiprogramación y el 
spooling para mejorar la eficiencia. El timesharing permitió una interacción más rápida al compartir la CPU entre múltiples usuarios. El proyecto 
MULTICS influyó en sistemas operativos futuros y mantuvo usuarios leales durante décadas. El concepto de "utilería para computadora" podría regresar 
con servidores masivos centralizados. La era de las minicomputadoras comenzó con la DEC PDP-11 y dio lugar a UNIX, desarrollado por Ken Thompson. 
UNIX influyó en la informática moderna, y Linux, inspirado en UNIX, se convirtió en un fenómeno del código abierto y sigue siendo influyente.

**4. La cuarta generacion (1980 hasta la fecha): las computadoras personales**
- En la década de 1970, el desarrollo de la tecnología LSI (Large Scale Integration) dio inicio a la era de las computadoras personales. Intel lanzó 
el microprocesador 8080 en 1974, y Gary Kildall desarrolló el sistema operativo CP/M para esta CPU. CP/M se convirtió en el sistema dominante en las 
microcomputadoras durante unos 5 años. Steve Jobs, co-fundador de Apple, se inspiró en la Interfaz Gráfica de Usuario (GUI) en Xerox PARC y lideró 
proyectos exitosos para desarrollar computadoras amigables para el usuario, como Lisa y Macintosh. Microsoft desarrolló Windows como un entorno 
gráfico sobre MS-DOS, y Windows 95 marcó el inicio de versiones independientes de Windows que utilizaban MS-DOS solo para iniciar programas 
antiguos. El mundo de las computadoras personales tiene dos competidores clave: UNIX y Windows. UNIX se usa en servidores y computadoras de 
escritorio, especialmente en países en desarrollo. Linux, una variante de UNIX, es una alternativa popular a Windows en computadoras con 
procesadores Pentium. Las redes de computadoras personales y los sistemas operativos distribuidos surgieron en la década de 1980, con desafíos como 
la ejecución simultánea en múltiples procesadores y la gestión de la comunicación.

