# Taller 3
## Ejercicios de Leetcode 

### Ejercicio 1: Two Sum
En el siguiente algoritmo desarrollado en c donde se realizo una matriz de números enteros nums y un número entero target, devuelva los índices de los dos números de modo que sumen target.

```C
# include <stdio.h>

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
    int *restar= malloc(2*sizeof(int));
    restar[0]= 1;
    restar[1]= 1;
    for (int i = 0; i<numsSize; ++i){
        for(int k = i+1; k<numsSize; ++k){
            if(nums[i]+nums[k]==target){
                *returnSize=2;
                restar[0] = i;
                restar[1] = k;
                return restar;
            }
        }
    }
    *returnSize=0;
    return NULL;
}

```
**Nota:**
- Tiempo de ejecucion: 76 ms
- Memoria: 6,32 MB

#### Algoritmo mas optimizado 
En el siguiente algoritmo desarrollado en distintos lenguajes se realizo una matriz que encuentra dos números en una lista que suman un número específico. Utiliza una "tabla" especial para recordar los números que se han visto y sus posiciones. Al recorrer la lista de números, busca si ya se tiene un número que, cuando lo sumamos a otro, nos da el número objetivo. Si lo encontramos, hemos hallado los dos números que se necesitan donde se utiliza una tabla hash para hacer esto de manera más rápida y eficiente. 

```C
typedef struct {
	int num;
	int idx;
	UT_hash_handle hh;
} num_t;

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
	int* res= calloc((*returnSize = 2), sizeof(int));
	num_t *refHash = NULL, *num = NULL, *tmp = NULL;

	for(int i = 0; i < numsSize; ++i){
		int finder = target - nums[i];
		HASH_FIND_INT(refHash, &finder, num);

		if (num != NULL){
			res[0] = num->idx;
			res[1] = i;

			break;
		} else {
			num = malloc(sizeof(num_t));
			num->num = nums[i]; num->idx = i;

			HASH_ADD_INT(refHash, num, num);
		}
	}

	HASH_ITER(hh, refHash, num, tmp) {
		HASH_DEL(refHash, num); free(num); 
	}

    return res;
}
```

**Nota:**
- Tiempo de ejecucion: 6 ms
- Memoria: 7,80 MB

### Ejercicio 2: Remove Duplicates from Sorted Array
En el siguiente algoritmo desarrollado en c se realizo una matriz de enteros nums ordenada en orden no decreciente , elimine los duplicados en el lugar de modo que cada elemento único aparezca solo una vez. El orden relativo de los elementos debe mantenerse igual. Luego devuelva la cantidad de elementos únicos en nums. 

```C
int removeDuplicates(int* nums, int numsSize) {
    if (numsSize <= 1) {
        return numsSize;  
    }
    
    int uniqueIndex = 1;  
    int currentElement = nums[0];  
    
    for (int i = 1; i < numsSize; i++) {
        if (nums[i] != currentElement) {
            nums[uniqueIndex] = nums[i];  
            currentElement = nums[i];  
            uniqueIndex++;
        }
    }
    
    return uniqueIndex;
}
```
**Nota:**
- Tiempo de ejecucion: 15 ms
- Memoria: 7,80 MB

#### Algoritmo mas optimizado
En el siguiente algoritmo mas optimizado desarrollado en c se resuelve el problema de eliminar números repetidos de una lista de números ordenados, manteniendo el orden y contando cuántos números únicos hay. Para hacerlo, utiliza dos apuntadores: uno avanza rápidamente para explorar la lista y otro avanza más lento para guardar los números únicos. Cuando el apuntador rápido encuentra un número nuevo, se coloca en la posición del apuntador lento, asegurando que solo los números únicos se almacenen en el orden correcto. Al final, el número de lugares donde está el apuntador lento más uno es la cantidad de números únicos en la lista.

```C
int removeDuplicates(int* nums, int numsSize) {
    if (numsSize <= 1) {
        return numsSize;  
    }
    
    int slow = 0; 
    int fast = 1;  
    
    while (fast < numsSize) {
        if (nums[fast] != nums[slow]) {
            slow++;  
            nums[slow] = nums[fast];  
        }
        fast++;  
    }
    
    return slow + 1; 
}
```
**Nota:**
- Tiempo de ejecucion: 14 ms
- Memoria: 7,80 MB

### Ejercicio 3: Remove Element
En el siguiente algoritmo desarrollado en c se realizo una matriz de enteros nums y un entero val, elimine todas las apariciones de val in nums -place. Se puede cambiar el orden de los elementos. Luego devuelva el número de elementos en nums los que no son iguales a val.

```C
int removeElement(int* nums, int numsSize, int val){
    int size = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] != val) {
            nums[size++] = nums[i];
        }
    }

    return size;
}
```

**Nota:**
- Tiempo de ejecucion: 2 ms
- Memoria: 6,30 MB

#### Algoritmo mas optimizado
En el siguiente algoritmo mas optimizado desarrollado en c se el problema de quitar un número especial de una lista de números. Donde se utilizo un enfoque simple: mientras recorre la lista, copia los números que no son iguales al número especial en la misma posición en la lista original. Al hacerlo, crea una nueva "hoja" con los números que quedan. Luego, devuelve cuántos números hay en esa hoja, lo que representa cuántos números quedan después de quitar los especiales.

```C
    int removeElement(int* nums, int numsSize, int val) {
    int size = 0;
    for (int i = 0; i < numsSize; i++) {
        if (nums[i] != val) {
            nums[size] = nums[i];
            size++;
        }
    }

    return size;
}
```


**Nota:**
- Tiempo de ejecucion: 1 ms
- Memoria: 5,94 MB

### Ejercicio 4: Median of Two Sorted Arrays
En el siguiente Algoritmo desarrollado en c se realizo para encontrar la mediana cuando se combinan dos grupos de números ordenados donde se utiliza una especie de juego de adivinanzas matemáticas para determinar dónde cortar los grupos de números de manera que queden equilibrados en ambos lados. Luego, la mediana es el número justo en medio de esos cortes. El código ajusta sus adivinanzas hasta encontrar el corte correcto. Si algo sale mal, muestra un mensaje de error.
```C
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    if (nums1Size > nums2Size) {
        return findMedianSortedArrays(nums2, nums2Size, nums1, nums1Size);
    }

    int x = nums1Size;
    int y = nums2Size;
    int low = 0;
    int high = x;

    while (low <= high) {
        int partitionX = (low + high) / 2;
        int partitionY = (x + y + 1) / 2 - partitionX;

        int maxX = (partitionX == 0) ? INT_MIN : nums1[partitionX - 1];
        int maxY = (partitionY == 0) ? INT_MIN : nums2[partitionY - 1];

        int minX = (partitionX == x) ? INT_MAX : nums1[partitionX];
        int minY = (partitionY == y) ? INT_MAX : nums2[partitionY];

        if (maxX <= minY && maxY <= minX) {
            if ((x + y) % 2 == 0) {
                return ((double) (fmax(maxX, maxY) + fmin(minX, minY))) / 2;
            } else {
                return (double) fmax(maxX, maxY);
            }
        } else if (maxX > minY) {
            high = partitionX - 1;
        } else {
            low = partitionX + 1;
        }
    }
    return -1; 
}

```

**Nota:**
- Tiempo de ejecucion: 10 ms
- Memoria: 6.32 MB

## link del blog
-[Link a mi blog](https://gitlab.com/Mars8719)

