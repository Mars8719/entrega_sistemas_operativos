# include <stdio.h>
# include <stdlib.h>
# include <time.h>

int main(void){
      int i, count=0, seed=100000;
      double x, y, pi;
      srand(time(NULL));
      for(i=0; i<seed; i++){
            x=(double)rand()/RAND_MAX;

            y=(double)rand()/RAND_MAX;

            double distance=x*x+y*y;

            if(distance <= 1.0){
                count++;
            }
      }
      pi = 4.0*count/seed;
      printf("El valor aproximado de PI es:%lf\n", pi);
      return 0;
}




