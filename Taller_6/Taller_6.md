# Taller N6 - Calculando PI

- Descargue los codigos del Ejemplo 07

- Compile: `mpicc -Wall -o ejecutable.exe main.c calculate_pi.c`

- Ejecute: `mpirun -np 2 ./ejecutable.exe 10000`, donde 1000 es N (muestras o puntos).

- Pruebe el codigo anterior con por lo menos 5 valores de N de diferente magnitud.

- Compare este codigo con el codigo en python visto previamente y su codigo desarrollado en lenguaje c y escriba sus conclusiones.


## Concluciones

<div style="text-align:justify;">

**Computación en Paralelo:**
se refiere a la ejecución simultánea de múltiples tareas o procesos de cómputo con el objetivo de resolver problemas de manera más rápida y eficiente que utilizando una sola unidad de procesamiento.

**Código `main.c`:**
En este se recibe un argumento desde la terminal al momento de ejecutarse, que representa la cantidad de muestras a generar para estimar π. Luego, este número de muestras se divide entre el número de tareas para distribuir la carga de trabajo entre los núcleos del equipo.

**Código `calculate_pi.c`:**
Dentro de este se encuentra la función `create_and_check_coordinates`. Esta función genera coordenadas aleatorias (x, y) dentro del rango [0,1) y verifica si estas coordenadas están dentro de un círculo. Además, se define una función llamada `do_checks` que utiliza la función anterior para realizar estas verificaciones y contar la cantidad de pares de coordenadas que caen dentro del círculo.

**Cálculo de π:**
en la anterior funcion se emplea las funciones anteriores para estimar el valor de π utilizando el método de Montecarlo con un número específico de intentos. Un aspecto importante es que cada tarea MPI inicia su propio generador de números aleatorios, utilizando una semilla que es igual al número de la tarea. Cada tarea también registra información sobre la cantidad de intentos realizados y su número de tarea correspondiente.

**Reducción de Resultados:**
Finalmente, para obtener una estimación global de π, se efectúa una operación de reducción utilizando la función `MPI_Allreduce`. Esta operación proporciona la suma global de los intentos que cayeron dentro del círculo (`N_in_global`) y el número total de intentos realizados (`N_global`). El valor estimado de π se calcula y se obtiene como `4 * (N_in_global / N_global)`.

Por ulimo, la computación en paralelo es una poderosa herramienta que permite realizar tareas computacionales de manera simultánea utilizando múltiples unidades de procesamiento. Ambos códigos, `main.c` y `calculate_pi.c`, trabajan en conjunto utilizando MPI para distribuir la carga de trabajo y calcular una estimación de π mediante el método de Montecarlo, con la tarea 0 en el primer código imprimiendo los resultados finales, incluido el valor estimado de π y el tiempo de ejecución.
</div>
