#include <stdio.h>

// Función para calcular la inversa de una matriz 2x2
int inversaMatriz2x2(double matriz[2][2], double inversa[2][2]) {
    double determinante = matriz[0][0] * matriz[1][1] - matriz[0][1] * matriz[1][0];

    if (determinante == 0) {
        printf("La matriz no tiene inversa.\n");
        return -1;
    }

    inversa[0][0] = matriz[1][1] / determinante;
    inversa[0][1] = -matriz[0][1] / determinante;
    inversa[1][0] = -matriz[1][0] / determinante;
    inversa[1][1] = matriz[0][0] / determinante;

    return 0;
}

int main() {
    //Definicion de espacio para una matriz 2x2 y su inversa
    double matriz[2][2];
    double inversa[2][2];

    printf("Ingrese los elementos de la matriz 2x2:\n");

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            printf("Elemento [%d][%d]: ", i, j);
            scanf("%lf", &matriz[i][j]);
        }
    }

    int resultado = inversaMatriz2x2(matriz, inversa); 
    //La funcion inversaMatriz2x2 retornara 0 si todo fue un exito
    //lo cual permitira entrar a imprimir los resultados. 

    if (resultado == 0) {
        printf("La matriz ingresada:\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                printf("%.2f\t", matriz[i][j]);
            }
            printf("\n");
        }

        printf("\nLa inversa de la matriz:\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                printf("%.2f\t", inversa[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}

