#include <stdio.h>

float funcion1(float x);
float funcion2(float x);
float funcion3(float x);
    
int main(void){
    float n, func_1, func_2, func_3;
    char opt;
    
    printf("Ingrese una opcion\n");
    scanf("%c", &opt);

    printf("Ingrese un numero decimal\n");
    scanf("%f", &n);
    
    switch(opt){
        case 'A':
            func_1=funcion1(n);
            printf("\n%f es el cuadrado de %f.\n", func_1,n);
            break;
        case 'B':
            func_2=funcion2(n);
            printf("\n%f es n**2 + 1000n de %f.\n", func_2,n);
            break;
        case 'C':
            func_3=funcion3(n);
            printf("\n%f es el caso 3 de %f.\n", func_3,n);
            break;
        default:
            printf("Funcion no valida\n");
            break;
    }
}


float funcion1(float x){
    return x*x;
}

float funcion2(float x){
    return (x*x) + 1000*x;
}

float funcion3(float x){
    float v = 0.0;
    if(x<=0){
        v=10;
    }

    if(x>0 && x<5){
        
        v=(x*x) - x+1;
    }

    if(x>=5){
        v=(2*x) - 1;
    }
    return v;
}
