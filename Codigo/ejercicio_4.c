#include <stdio.h>

float area_pies(float x, float y);
float area_metros(float x, float y);

int main(void){

    float ancho,largo, uni_pies, uni_metros;
    int optn;
    
    
    printf("En que unidad desea el area: \n");
    printf("1. Pies^2 \n");
    printf("2. Metros^2 \n");
    scanf("%d", &optn);
        
    printf("Ingrese el largo en metros: \n");
    scanf("%f",&largo);

    printf("Ingrese el ancho en metros: \n");
    scanf("%f", &ancho);


    switch(optn){
        case 1:
            uni_pies=area_pies(ancho,largo);
            printf("El tamaño de la habitacion es de: %f Ft^2 \n",uni_pies);
            break;
        case 2:
            uni_metros=area_metros(ancho,largo);
            printf("El tamaño de la habitacion es de: %f M^2 \n",uni_metros);
            break;
    }
}

float area_pies(float x, float y){
    float ft,valor_area;
    ft=10.7639;
    valor_area = (x*y)*ft;
    return valor_area;
}


float area_metros(float x,float y){
    float valor_area;
    valor_area = x*y;
    return valor_area;
}
