#include <stdio.h>

float normalize(float x, float xmin, float xmax, float a, float b){
    return a + ((x - xmin) * (b - a)) / (xmax -xmin);
}

int main(){
    int A[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

    //Valores maximo y minimo
    float xmax= 421;
    float xmin= 3;
    //[-1, 1]
    float a1 = -1.0;
    float b1 = 1.0;
    
    //[1, 10]
    float a2 = 1.0;
    float b2 = 10.0;

    //[0.5, 1]
    float a3 = 0.5;
    float b3 = 1.0;
    
    printf("Intervalo [-1, 1]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a1, b1);
        printf("%.2f ", x_norm);
    }

    printf("\n Intervalo [1, 10]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a2, b2);
        printf("%.2f ", x_norm);
    }
    
    printf("\n Intervalo [0.5, 1]: \n");
    for (int i = 0; i < 20; i++){
        float x_norm = normalize(A[i], xmin, xmax, a3, b3);
        printf("%.2f ", x_norm);
    }
    
    return 0;
}
