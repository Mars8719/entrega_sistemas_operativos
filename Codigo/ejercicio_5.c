#include <stdio.h>

float area_pies(float x, float y);
float conv_acres(float x, float y);

int main(void){
    float longi, ancho, area_pie, conv_ac;
    int opt;

    printf("\n Elige un procedimiento a realizar: \n");
    printf("\n 1. area en pies cuadrados \n");
    printf("\n 2. convertir un pie cuadrado a acres \n");
    scanf("%d", &opt);

    printf("Ingrese el longitud en metros: \n");
    scanf("%f", &longi);

    printf("Ingrese el ancho en metros: \n");
    scanf("%f", &ancho);

    switch(opt){
        case 1: 
            area_pie=area_pies(longi,ancho);
            printf("El area en pies es de: %f Ft^2 \n", area_pie);
            break;
        case 2:
            conv_ac=conv_acres(longi,ancho);
            printf("Los pies al cuadrado en acres son: %f acres \n", conv_ac);
            break; 
    }

}

float area_pies(float x, float y){
    float ft, valor_area;
    ft=10.7639;
    valor_area = (x*y)*ft;
    return valor_area;
}

float conv_acres(float x, float y){
    float ft, valor_acres;
    ft=10.7639;
    valor_acres=((x*y)*ft)/43560;
    return valor_acres;
}

