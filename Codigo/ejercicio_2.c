#include <stdio.h>

void converCel(void){
    float conv=0.0;
    float valconv;
    printf("Ingrese el valor a convertir: ");
    scanf("%f", &valconv);
    conv=(valconv-32)*(5/9);
    printf("\n%f es la conversion de %f en grados Celcius", valconv, conv);

}

void converFahr(void){
    float conv=0.0;
    float valconv;
    float mixta;
    printf("Ingrese el valor a convertir: ");
    scanf("%f", &valconv);
    mixta=57.6;
    conv=valconv+mixta;
    printf("\n%f es la conversion de %f en grados Fahrenheit", valconv, conv);

}

int main(void){
    int opt;
    printf("Ingrese la tarea que desea realizar: \n");
    printf("1) para convertir de fahrenheit a celcius. \n");
    printf("2) para convertir de celsius a fahrenheit. \n");
    scanf("%d", &opt);
    if(opt==1){
        converCel();
    }
    if(opt==2){
        converFahr();
    }
    return 0;
}
