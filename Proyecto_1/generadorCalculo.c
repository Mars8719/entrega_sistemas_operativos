#include <stdio.h>
#include <math.h>

// Parámetros del oscilador armónico
double mass = 1.0;
double omega = 1.0;
double hbar = 1.0;

// Función para calcular las funciones de onda
double wave_function(int n, double x) {
    double prefactor = 1 / (sqrt(pow(2, n) * tgamma(n + 1)));
    return prefactor * sqrt(mass * omega / (M_PI * hbar)) * exp(-0.5 * mass * omega / hbar * x * x);
}

int main() {
    // Abrir un archivo para escritura
    FILE *file = fopen("resultados.txt", "w");
    if (file == NULL) {
        printf("Error al abrir el archivo.\n");
        return 1;
    }

    // Definir los valores de n y x para los que deseas calcular los resultados
    int max_n = 3;
    double x_min = -5.0;
    double x_max = 5.0;
    double dx = 0.01;

    for (int n = 0; n <= max_n; n++) {
        for (double x = x_min; x <= x_max; x += dx) {
            double psi = wave_function(n, x);
            double probability = psi * psi;
            double potential = 0.5 * mass * omega * omega * x * x;

            // Escribir los resultados en el archivo en el formato especificado
            fprintf(file, "n = %d, x = %.4f, Psi = %.6f, Probability = %.6f, Potential = %.6f\n", n, x, psi, probability, potential);
        }
    }

    // Cerrar el archivo
    fclose(file);

    printf("Resultados guardados en resultados.txt\n");

    return 0;
}
