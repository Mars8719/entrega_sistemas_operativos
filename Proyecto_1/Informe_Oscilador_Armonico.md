# Informe sobre la Construcción de un Oscilador Armónico utilizando el Método de Numerov en Python

## Introducción:
- El oscilador armónico es un sistema físico ampliamente estudiado que describe el comportamiento de un objeto que se mueve bajo la influencia de una fuerza restauradora proporcional a su desplazamiento desde una posición de equilibrio. El Método de Numerov es una técnica numérica utilizada para resolver ecuaciones diferenciales de segundo orden, como la ecuación del oscilador armónico.

## Desarrollo:

### 1. Implementación en C utilizando el Método de Numerov:

A continuación, se muestra en el lenguaje en C la implementacion de una solucion para el oscilador armonico que utiliza el Método de Numerov y genera un archivo de texto (.txt) con los valores de posición y velocidad.

```C
#include <stdio.h>
#include <math.h>

// Parámetros del oscilador armónico
double mass = 1.0;
double omega = 1.0;
double hbar = 1.0;

// Función para calcular las funciones de onda
double wave_function(int n, double x) {
    double prefactor = 1 / (sqrt(pow(2, n) * tgamma(n + 1)));
    return prefactor * sqrt(mass * omega / (M_PI * hbar)) * exp(-0.5 * mass * omega / hbar * x * x);
}

int main() {
    // Abrir un archivo para escritura
    FILE *file = fopen("resultados.txt", "w");
    if (file == NULL) {
        printf("Error al abrir el archivo.\n");
        return 1;
    }

    // Definir los valores de n y x para los que deseas calcular los resultados
    int max_n = 3;
    double x_min = -5.0;
    double x_max = 5.0;
    double dx = 0.01;

    for (int n = 0; n <= max_n; n++) {
        for (double x = x_min; x <= x_max; x += dx) {
            double psi = wave_function(n, x);
            double probability = psi * psi;
            double potential = 0.5 * mass * omega * omega * x * x;

            // Escribir los resultados en el archivo en el formato especificado
            fprintf(file, "n = %d, x = %.4f, Psi = %.6f, Probability = %.6f, Potential = %.6f\n", n, x, psi, probability, potential);
        }
    }

    // Cerrar el archivo
    fclose(file);

    printf("Resultados guardados en resultados.txt\n");

    return 0;
}

```

### 2. Graficación de los Valores Normales y Cuadráticos:

Para graficar los valores de posición y velocidad se utilizo el lenguaje python, así como sus valores cuadráticos, puede se hizo uso de Matplotlib. 

```py
import numpy as np
import matplotlib.pyplot as plt

# Leer los valores desde el archivo "respuestas.txt"
with open("resultados.txt", "r") as file:
    lines = file.readlines()

n_values, x_values, psi_values, probability_values, potential_values = [], [], [], [], []

for line in lines:
    parts = line.strip().split(', ')
    n = int(parts[0].split('=')[1])
    x = float(parts[1].split('=')[1])
    psi = float(parts[2].split('=')[1])
    probability = float(parts[3].split('=')[1])
    potential = float(parts[4].split('=')[1])

    n_values.append(n)
    x_values.append(x)
    psi_values.append(psi)
    probability_values.append(probability)
    potential_values.append(potential)

# Crear subplots para graficar
plt.figure(figsize=(12, 8))

for n in range(max(n_values) + 1):
    plt.subplot(2, 2, n + 1)
    indices = [i for i, n_val in enumerate(n_values) if n_val == n]
    plt.plot([x_values[i] for i in indices], [psi_values[i] for i in indices], label=f'n = {n}')
    plt.plot([x_values[i] for i in indices], [probability_values[i] for i in indices], label=f'n = {n} Density')
    plt.plot(x_values, potential_values, label='Potencial', linestyle='--')
    plt.title(f'Función de Onda, Densidad de Probabilidad y Potencial para n = {n}')
    plt.xlabel('x')
    plt.grid(True)
    plt.legend()

plt.tight_layout()
plt.show()
```

## Conclusion

Se ha construido un oscilador armónico utilizando el Método de Numerov en el lenguaje c y generado un archivo .txt con los valores de posición y velocidad. Luego, se ha graficado en el lenguaje python tanto los valores normales como los cuadráticos de posición y velocidad utilizando Matplotlib. Este enfoque permite estudiar el comportamiento del oscilador armónico de manera numérica y visual.